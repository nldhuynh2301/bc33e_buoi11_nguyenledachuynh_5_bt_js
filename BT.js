// BT1: Tinh tien luong nhan vien
// Input: 
// Tien lương 1 ngày 100.000
// So ngay lam: người dùng nhập.Tạo 3 biến tienLuong, soNgay, luong1Ngay

var tienLuong = 0;
var soNgay = 26;
var luong1Ngay = 100000;

// Cong thu tinh tien luong nhan vien:
// Tạo biến tienLuong tính bằng công thức Tiền lương = số ngày làm * lương 1 ngày

tienLuong = soNgay * luong1Ngay;
console.log("BT1: TÍNH TIỀN LƯƠNG NHÂN VIÊN: ");

console.log("Tiền lương 1 ngày: ", luong1Ngay);
console.log("Số ngày làm việc: ", soNgay);
console.log("Tiền lương: ", tienLuong);

// BT2: TÍNH GIÁ TRỊ TRUNG BÌNH 5 SỐ THỰC
console.log("------------------------------------------");

console.log("BT2: TÍNH GIÁ TRỊ TRUNG BÌNH 5 SỐ THỰC: ");
// Input: 5 GIÁ TRỊ SỐ THỰC
// Tạo 5 biến chứa giá trị số thực: so1, so2, so3, so4, so5:

var so1 = 4;
var so2 = 5;
var so3 = 20;
var so4 = 39;
var so5 = 20;

//  Tính giá trị trung bình cộng 5 số : (so1+so2+so3+so4+so5)/5
// OUTPUT trung bình cộng 5 số thực:
// Tạo 1 biến trungBinh và tính giá trị biến

var trungBinh = (so1 + so2 +so3 + so4 +so5) / 5;
console.log("Gía trị số thứ 1:", so1);
console.log("Gía trị số thứ 2:", so2);
console.log("Gía trị số thứ 3:", so3);
console.log("Gía trị số thứ 4:", so4);
console.log("Gía trị số thứ 5:", so5);
console.log("Giá trị trung bình cộng 5 số thực là:",trungBinh );
console.log("------------------------------------------");

// BT3: Quy Đổi Tiền USD:
console.log("BT3: Quy Đổi Tiền USD: ");
// INPUT Giá usd:23500, người dùng nhập số tiền vào
// Tạo 2 biến giaUsd, sotienUsd

var giaUsd = 23500;
var sotienUsd = 20;

// OUTPUT: Số tiền vnd khi đổi sang USD
// tạo biến tienVnd

var tienVnd = giaUsd * sotienUsd;
console.log("Giá tiền USD hiện tại: ", giaUsd);
console.log("Số tiền USD cần đổi sang VND: ",sotienUsd );
console.log("Giá tiền VND đổi từ USD: ", tienVnd);
console.log("------------------------------------------");

// BT4: Tính diện tích, chu vi hình chữ nhật:
console.log("BT4: Tính diện tích, chu vi hình chữ nhật:")
// INPUT: Chieu dai va chieu rong hinh chu nhat
// Tạo 2 biến chieuDai và chieuRong

var chieuDai = 10;
var chieuRong = 5;
console.log("Giá trị chiều dài HCN:", chieuDai);
console.log("Giá trị chiều rộng HCN:", chieuRong);

// OUTPUT: Dien Tich va Chu Vi Hinh chu nhat: tạo biến dienTich, chuVi
var dienTich = chieuDai * chieuRong;
var chuVi = ( chieuDai + chieuRong ) * 2;
console.log("Diện tích: ", dienTich);
console.log("Chu Vi: ", chuVi);
console.log("------------------------------------------");

// BT5: Tính tổng 2 ký số:
console.log("BT5: Tính tổng 2 ký số: ");
// INPUT: nhập 1 số có 2 chữ số, tạo 3 biến: chuSo, hangChuc, donVi

var chuSo = 45;
console.log("Số cần tính là: ", chuSo);

var hangChuc = Math.floor( chuSo % 100 / 10 );
var donVi = chuSo % 10;

console.log("Số hàng chục là: ", hangChuc);
console.log("Số hàng đơn vị là: ", donVi);
// OUTPUT: chữ số hàng chục + chữ số hàng đơn vị: tạo biến Tong

var Tong = hangChuc + donVi;
console.log("Tổng hai ký số là: ", Tong);
















